
import AES from 'zunzun/flypto/aes.mjs'

import CSRF_MG from '../csrf/mg.mjs'

export default class CredentialParser {
  constructor(dev_fp, fp_salt) {
    if (typeof dev_fp !== "string") dev_fp = dev_fp.toString();

    const time_a = Date.now();
    console.log("FP KEY", dev_fp, fp_salt, 1000);
    const fp_aes = this.fp_aes = new AES(AES.derive_key(dev_fp, fp_salt, 1000));
    console.log("Derive key", Date.now() - time_a);

    const flyauth_session_key_enc = localStorage.getItem(AUTHING_PREFIX+"flyauth_session_key");

    let flyauth_session_key = undefined;
    try {
      flyauth_session_key = fp_aes.decrypt(flyauth_session_key_enc);
    } catch (e) {
      localStorage.removeItem(AUTHING_PREFIX+"flyauth_session_key");
    }
    console.log("session key", flyauth_session_key);

    if (flyauth_session_key) {
      const session_aes = new AES(flyauth_session_key);

      this.creds = AUTHING_CREDENTIALS.enc ? JSON.parse(session_aes.decrypt(AUTHING_CREDENTIALS.enc)) : {};
      this.creds.fingerprint = dev_fp;
      this.creds.oaes = new AES(this.creds.oaes);
      this.creds.iaes = new AES(this.creds.iaes);


      console.log("this.cred", this.creds);

      if (!this.creds.cks) {
        this.no_session = true;
      }

      if (typeof AUTHING_CREDENTIALS.next_csrf !== 'function') {
        const csrf_mg = new CSRF_MG(this.creds, session_aes, {
          authing_url: AUTHING_PATH
        });
        AUTHING_CREDENTIALS.next_csrf = csrf_mg.next_csrf;
      }

    } else {
      this.no_session = true;
    }

  }

  async next(data) {
    if (this.no_session) return undefined;
    const creds = { ...this.creds }

    const cks = creds.cks;
    delete creds.cks;

    creds.csrf_secret = await AUTHING_CREDENTIALS.next_csrf();

    if (data) creds.data = data;

    return {
      cks: cks,
      authorized: encodeURIComponent(creds.oaes.encrypt(JSON.stringify(creds)))
    }
  }

}
