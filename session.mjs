
import AES from 'zunzun/flypto/aes.mjs'
import RSA from 'zunzun/flypto/rsa.mjs'


export default class Session {

  static initialize(creds) {
    const rsa = new RSA(creds.pub_key.replace(/\\n/g, "\r\n"));

    const key_aes = new AES();
    creds.key = key_aes.key;
    creds.iaes = new AES();

    const authorized = encodeURIComponent(
      rsa.encrypt(
        JSON.stringify({
          key: key_aes.key,
          secret: creds.iaes.key,
          fingerprint: creds.fingerprint
        })
      )
    );

    return `authorized=${authorized}`;
  }

  static decrypt_init(enc_sess_data, creds) {
    const sess_data = JSON.parse(creds.iaes.decrypt(enc_sess_data));
    for (let prop in sess_data) {
      if (prop == "oaes") {
        creds[prop] = new AES(sess_data[prop]);
      } else {
        creds[prop] = sess_data[prop]
      }
    }
    return sess_data;
  }

  static authenticate(values, creds) {
    values.fingerprint = creds.fingerprint;
    values.tab_id = creds.tab_id;
    values.csrf_secret = creds.csrf_secret;
    const authorized = encodeURIComponent(
      creds.oaes.encrypt(
        JSON.stringify(values)
      )
    );
    return `cks=${creds.cks}&authorized=${authorized}`
  }

  static decrypt_auth(enc_sess_data, creds) {
    const sess_data = JSON.parse(creds.iaes.decrypt(enc_sess_data));
    for (let prop in sess_data) {
      creds[prop] = sess_data[prop]
    }
    return sess_data;
  }

  static credentials(creds, data) {
    const credentials = {
      fingerprint: creds.fingerprint,
      tab_id: creds.tab_id,
      csrf_secret: creds.csrf_secret
    };
    if (data) credentials.data = data;
    return `cks=${creds.cks}&authorized=${encodeURIComponent(creds.oaes.encrypt(JSON.stringify(credentials)))}`;
  }

}
