
import AES from 'zunzun/flypto/aes.mjs'

export default class CSRFManager {
  constructor(creds, session_aes, cfg) {
    this.ots_list = creds.csrf_secrets ? [...creds.csrf_secrets] : [];
    delete creds.csrf_secrets;
    this.creds = creds;

    this.session_aes = session_aes;

    this.next_csrf = async () => {
      return await this.next();
    };

    this.cfg = cfg;
  }

  async check() {
    const _this = this;
    if (this.fetching) {
      await new Promise((fulfil) => {
        setInterval(() => {
          if (!_this.fetching) {
            fulfil();
          }
        }, 100);
      })
    }

    if (this.ots_list.length === 1) {
      this.fetching = true;

      const creds = {...this.creds};

      const cks = creds.cks;


      const enc_aes = new AES(creds.enc_key);

      const secret_aes = new AES();

      const credentials = {
        fingerprint: creds.fingerprint,
        tab_id: creds.tab_id,
        csrf_secret: this.ots_list.shift(),
      };

      const auth_str = encodeURIComponent(creds.oaes.encrypt(JSON.stringify(credentials)));
      const path_prefix = this.cfg.authing_url == "/" ? "" : this.cfg.authing_url;
      console.log("POST >", path_prefix+"/flyauth/last-ots", "+", `cks=${cks}&authorized=${auth_str}`);

      const new_ots_enc = await fetch(path_prefix+"/flyauth/last-ots", {
        method: 'POST',
        body: `cks=${cks}&authorized=${auth_str}`,
        headers: this.cfg.header_fn ? this.cfg.header_fn() : undefined 
      }).then( (response) => response.text() );

      const sa_decrypted = creds.iaes.decrypt(
        new_ots_enc
      )

      const tsa_decrypted = this.session_aes.decrypt(
        sa_decrypted
      )
    
      this.ots_list = JSON.parse(tsa_decrypted);
      this.fetching = false;
    }
  }

  async next() {
    await this.check();
    return this.ots_list.shift();
  }
}
