
import AES from 'zunzun/flypto/aes.mjs'
import RSA from 'zunzun/flypto/rsa.mjs'

export default class Auth {
  static async enticate(pub_key, fingerprint, config, values) {
    const rsa = new RSA(pub_key.replace(/\\n/g, "\r\n"));

    let secret_aes = new AES();

    const key_aes = new AES();
    const session_key = key_aes.key;

    const authorized_param = rsa.encrypt(JSON.stringify({
      secret: secret_aes.key,
      fingerprint: fingerprint,
      key: session_key
    }));

    const enc_sess_data = await fetch(config.path_prefix+'/flyauth/init-session', {
      method: 'POST',
      body: `authorized=${authorized_param}`
    }).then(response => {
      if (config.cookie_store) config.cookie_store.parse_response(response);
      return response.text();
    });

    const sess_data = JSON.parse(secret_aes.decrypt(enc_sess_data));

    let enc_aes = new AES(sess_data.enc_key);

    secret_aes = new AES();
    values.secret = secret_aes.key;
    values.fingerprint = fingerprint;
    values.tab_id = sess_data.tab_id;
    values.csrf_secret = sess_data.csrf_secret;

    const signin_req_cfg = {
      method: 'POST',
      body: `cks=${sess_data.cks}&authorized=${encodeURIComponent(enc_aes.encrypt(JSON.stringify(values)))}`
    }

    if (config.cookie_store) {
      signin_req_cfg.headers = {
        'Cookie': config.cookie_store.get_header()
      }
    }

    const response = await fetch(config.path_prefix+'/flyauth/signin', signin_req_cfg).then((response) => {
      if (response.status == 401) {
        step_form.err_cb(lang[lkey].signin.error.auth_error);
        return false;
      } else if (response.status == 403) {
        return response.blob();
      } else {
        return response.text()
      }
    });
    return {
      secret_aes: secret_aes,
      sess_data: sess_data,
      enc_aes: enc_aes,
      session_key: session_key,
      string: response
    }
  }

}
